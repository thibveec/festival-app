<?php namespace Api\Controllers;

use Festival, Response, Input;

class FestivalController extends \BaseController {

    public $restful = true;

    public function get_index()
    {

        return Response::json(Festival::all())->setCallback(Input::get('callback'));

    }

    public function post_index()
    {

        $newtodo = Input::json();

        $todo = new Festival();
        $todo->title = $newtodo->title;
        $todo->completed = $newtodo->completed;
        $todo->save();

        return Response::eloquent($todo);
    }

    public function put_index()
    {
        $updatetodo = Input::json();

        $todo = Festival::find($updatetodo->id);
        if(is_null($todo)){
            return Response::json('Festival not found', 404);
        }
        $todo->title = $updatetodo->title;
        $todo->completed = $updatetodo->completed;
        $todo->save();
        return Response::eloquent($todo);
    }

    public function delete_index($id = null)
    {
        $todo = Festival::find($id);

        if(is_null($todo))
        {
            return Response::json('Festival not found', 404);
        }
        $deletedtodo = $todo;
        $todo->delete();
        return Response::eloquent($deletedtodo);
    }

}
(function(){
    'use strict';

    var controllers = angular.module('ddsApp.controllers');

    controllers.controller('ddsApp.controllers.FestivalCtrl',['$scope', '$routeParams', 'ddsApp.services.WeatherSrvc', 'festivals', function($scope, $routeParams, WeatherSrvc, festivals){
        var festivalId = $routeParams.festivalId;
        $scope.festival = _.find(festivals, {'id':festivalId});



        $scope.isFestivalChosen = WeatherSrvc.isFestivalAlreadyChosen(festivalId);

        $scope.addChosenFestival = function(){
            if(!WeatherSrvc.isFestivalAlreadyChosen(festivalId)){
                WeatherSrvc.addFestivalToChoice(festivalId);
                $scope.isFestivalChosen = true;
            }
        };

        $scope.removeChosenFestival = function(){
            if(WeatherSrvc.isFestivalAlreadyChosen(festivalId)){
                WeatherSrvc.removeFestivalFromChoice(festivalId);
                $scope.isFestivalChosen = false;
            }
        };
    }]);
})();

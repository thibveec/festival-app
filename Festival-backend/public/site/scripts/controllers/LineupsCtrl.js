(function(){
    'use strict';

    var controllers = angular.module('ddsApp.controllers');

    controllers.controller('ddsApp.controllers.LineupsCtrl',['$scope', '$routeParams', 'ddsApp.services.WeatherSrvc', 'lineups', 'festivalchoice', function($scope, $routeParams, WeatherSrvc, lineups, festivalchoice){


        $scope.lineups = lineups;
        $scope.givefestival = festivalchoice.toString();
       $scope.givenfestivals = _.filter(lineups, {'festival_id':$scope.givefestival});



        $scope.isList = true;
        $scope.changeIsList = function(isList){
            $scope.isList = isList;
            $scope.lflrefresh = !isList;
        };

        $scope.isAlreadyInSchedule = function(lineupId){
            return WeatherSrvc.isLineupAlreadyInSchedule(lineupId.toString());
        };
        $scope.isNotInSchedule = function(lineupId){
            return WeatherSrvc.isLineupNotInSchedule(lineupId.toString());
        };

        $scope.addToSchedule = function(lineupId){

            if(!WeatherSrvc.isLineupAlreadyInSchedule(lineupId)){
                WeatherSrvc.addLineupToSchedule(lineupId);
                WeatherSrvc.isAlreadyInSchedule = true;

            }

        };



        $scope.removeFromSchedule = function(lineupId){

            if(WeatherSrvc.isLineupAlreadyInSchedule(lineupId)){

                WeatherSrvc.removeLineupFromSchedule(lineupId);
                WeatherSrvc.isAlreadyInSchedule = false;
            }


        };

    }]);
})();

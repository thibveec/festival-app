(function(){
    'use strict';

    var controllers = angular.module('ddsApp.controllers');

    controllers.controller('ddsApp.controllers.FestivalsCtrl',['$scope', '$routeParams', 'ddsApp.services.WeatherSrvc', 'festivals',  function($scope, $routeParams, WeatherSrvc, festivals){



        $scope.festivals = festivals;

        $scope.isList = true;
        $scope.changeIsList = function(isList){
            $scope.isList = isList;
            $scope.lflrefresh = !isList;
        };

        $scope.isFestivalChosen = function(festivalId){
            return WeatherSrvc.isFestivalAlreadyChosen(festivalId.toString());
        };



    }]);
})();

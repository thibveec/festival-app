(function(){
    'use strict';

    var controllers = angular.module('ddsApp.controllers');

    controllers.controller('ddsApp.controllers.AgendaCtrl',['$scope',  'ddsApp.services.WeatherSrvc', 'schedule', 'lineups',  function($scope,  WeatherSrvc, schedule, lineups){

        $scope.lineups = lineups;
        console.log(schedule);
        $scope.givelineup = schedule.toString();

        $scope.givenlineups = [];
        _.forEach(schedule, function(lineup){
          $scope.inschedule = _.filter(lineups, {'id':lineup});
           $scope.givenlineups.push($scope.inschedule);
        });
        console.log($scope.givenlineups);


        $scope.isList = true;
        $scope.changeIsList = function(isList){
            $scope.isList = isList;
            $scope.lflrefresh = !isList;
        };

    }]);
})();

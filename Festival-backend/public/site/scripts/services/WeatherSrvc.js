(function(){
    'use strict';

    var services = angular.module('ddsApp.services');

    services.factory('ddsApp.services.WeatherSrvc',
        ['$rootScope', '$http', '$q', '$window', 'localStorageService', function($rootScope, $http, $q, $window, localStorageService){
            var URLWEATHER = ' https://api.forecast.io/forecast/{0}/{1},{2}?callback=JSON_CALLBACK&units=ca';
            var URLFESTIVAL = 'http://localhost:8080/FestivalGadget/Festival-backend/public/api/v1/festivals?callback=JSON_CALLBACK';
            var URLLINEUPS = 'http://localhost:8080/FestivalGadget/Festival-backend/public/api/v1/lineups?callback=JSON_CALLBACK';
            var URLCOLORS = 'http://localhost:8080/FestivalGadget/Festival-backend/public/api/v1/colors?callback=JSON_CALLBACK';
            var MSGWEATHERLOADERROR = 'Could not load the Weather call';
            var MSGFESTIVALERROR = 'Could not load festivals';
            var MSGLINEUPERROR = 'Could not load lineups';
            var MSGCOLORSERROR = 'Could not load colors';
            var MSGGEOLOCATIONNOTSUPPORTED = 'GEO Location not supported';

            var _festivals = null,
                _lineups = null,
                _schedule = null,
                _chosenfestival = null,
                _colors = null,
                _themecolor = null,
                _numberOfResourcesToLoadViaAJAX = 2,
                _numberOfResourcesLoadedViaAJAX = 0;

            var _configuration, _geoPosition;

            var that = this;//Hack for calling private functions and variables in the return statement

            this.loadFestivals = function(){
                var deferred = $q.defer();

                if(_festivals === null){
                    if(localStorageService.get('festivals') === null){
                        $http.jsonp(URLFESTIVAL).
                            success(function(data, status, headers, config){
                                _festivals = data;
                                console.log(data);
                                deferred.resolve(_festivals);
                            }).
                            error(function(data, status, headers, config){
                                deferred.reject(MSGWEATHERLOADERROR);
                                console.log(data + ' ' + status + ' ' + headers);
                            });

                    }else{
                        _festivals = localStorageService.get('festivals');
                        deferred.resolve(_festivals);
                    }
                }else{
                    deferred.resolve(_festivals);
                }

                return deferred.promise;//Always return a promise
            };

            this.loadLineups = function(){
                var deferred = $q.defer();

                if(_lineups === null){
                    if(localStorageService.get('lineups') === null){
                        $http.jsonp(URLLINEUPS).
                            success(function(data, status, headers, config){
                                _lineups = data;
                                console.log(data);
                                deferred.resolve(_lineups);
                            }).
                            error(function(data, status, headers, config){
                                deferred.reject(MSGLINEUPERROR);
                                console.log(data + ' ' + status + ' ' + headers);
                            });

                    }else{
                        _lineups = localStorageService.get('lineups');
                        deferred.resolve(_lineups);
                    }
                }else{
                    deferred.resolve(_lineups);
                }

                return deferred.promise;//Always return a promise
            };
            this.loadColors = function(){
                var deferred = $q.defer();

                if(_colors === null){
                    if(localStorageService.get('colors') === null){
                        $http.jsonp(URLCOLORS).
                            success(function(data, status, headers, config){
                                _colors = data;
                                console.log(data);
                                deferred.resolve(_colors);
                            }).
                            error(function(data, status, headers, config){
                                deferred.reject(MSGCOLORSERROR);
                                console.log(data + ' ' + status + ' ' + headers);
                            });

                    }else{
                        _colors = localStorageService.get('colors');
                        deferred.resolve(_colors);
                    }
                }else{
                    deferred.resolve(_colors);
                }

                return deferred.promise;//Always return a promise
            };

            this.geoLocation = function(){
                if(Modernizr.geolocation){
                    var options = {maximumAge:60000, timeout:10000,enableHighAccuracy:true};

                    $window.navigator.geolocation.getCurrentPosition(this.geoLocationSuccess, this.geoLocationError,options);
                }else{
                    this.geoLocationFallback();
                }
            };

            this.geoLocationSuccess = function(position){
                _geoPosition = position;
                $rootScope.$broadcast("GEOSUCCESS");
            };

            this.geoLocationError = function(error){
                switch(error){
                    //Timeout
                    case 3:
                        geoLocation();
                        break;
                    //POSITION UNAVAILABLE
                    case 2:
                        geoLocation();
                        break;
                    //PERMISSION DENIED --> FALLBACK
                    case 1:
                        geoLocationFallback();
                        break;
                    default:
                        geoLocation();
                        break;
                }
            };

            this.geoLocationFallback = function(){
                $rootScope.$broadcast("GEOERROR");

            };

            this.loadDataChoosenFestival = function(){
                if(_chosenfestival === null){
                    if(localStorageService.get('chosenfestival') === null){
                        _chosenfestival = [];
                    }else{
                        _chosenfestival = localStorageService.get('chosenfestival');
                    }
                }
            };
            this.loadDataChoosenLineups = function(){
                if(_schedule === null){
                    if(localStorageService.get('schedule') === null){
                        _schedule = [];
                    }else{
                        _schedule = localStorageService.get('schedule');
                    }
                }
            };
            this.loadTheme = function(){
                if(_themecolor === null){
                    if(localStorageService.get('themecolor') === null){
                        _themecolor = [];
                    }else{
                        _themecolor = localStorageService.get('themecolor');
                    }
                }
            };


            return{
                loadConfiguration:function(){
                    var deferred = $q.defer();

                    that.loadDataChoosenFestival();
                    that.loadDataChoosenLineups();
                    that.loadTheme();

                    that.loadFestivals().then(
                        function(data){
                            _numberOfResourcesLoadedViaAJAX++;
                            if(_numberOfResourcesLoadedViaAJAX === _numberOfResourcesToLoadViaAJAX){
                                deferred.resolve(true);
                            }
                        },
                        function(error){
                            deferred.reject(MSGFESTIVALERROR);
                        }
                    );
                    that.loadLineups().then(
                        function(data){
                            _numberOfResourcesLoadedViaAJAX++;
                            if(_numberOfResourcesLoadedViaAJAX === _numberOfResourcesToLoadViaAJAX){
                                deferred.resolve(true);
                            }
                        },
                        function(error){
                            deferred.reject(MSGLINEUPERROR);
                        }
                    );
                    that.loadColors().then(
                        function(data){
                            _numberOfResourcesLoadedViaAJAX++;
                            if(_numberOfResourcesLoadedViaAJAX === _numberOfResourcesToLoadViaAJAX){
                                deferred.resolve(true);
                            }
                        },
                        function(error){
                            deferred.reject(MSGCOLORSERROR);
                        }
                    );

                    return deferred.promise;//Always return a promise
                },
                loadWeather : function(api, lat, long){
                    var deferred = $q.defer();

                    var url = URLWEATHER.format(api, lat, long);

                    $http.jsonp(url).
                        success(function(data, status, headers, config){
                            console.log(data);
                            deferred.resolve(data);
                        }).
                        error(function(data, status, headers, config){
                            deferred.reject(MSGWEATHERLOADERROR);
                            console.log(data + ' ' + status + ' ' + headers);
                        });

                    return deferred.promise;//Always return a promise
                },
                getDataFestivals : function(){
                    var deferred = $q.defer();



                    if(_festivals === null){
                        deferred.reject(MSGFESTIVALERROR);
                    }else{
                        deferred.resolve(_festivals);
                    }

                    return deferred.promise;//Always return a promise
                },
                getDataLineups : function(){
                    var deferred = $q.defer();



                    if(_lineups === null){
                        deferred.reject(MSGLINEUPERROR);
                    }else{
                        deferred.resolve(_lineups);
                    }

                    return deferred.promise;//Always return a promise
                },
                getDataColors : function(){
                    var deferred = $q.defer();



                    if(_colors === null){
                        deferred.reject(MSGCOLORSERROR);
                    }else{
                        deferred.resolve(_colors);
                    }

                    return deferred.promise;//Always return a promise
                },
                getChosenFestival:function(){
                    var deferred = $q.defer();

                    var choice = [];
                    var chosen = localStorageService.get('chosenfestival');
                    if(chosen !== null){
                        _.each(chosen, function(id){
                            var fest = _.find(chosen, function(festival){
                                return festival.id === id;
                            });
                            if(typeof fest !== 'undefined')
                                choice.push(fest);
                        });
                        deferred.resolve(chosen);
                    }else{
                        deferred.reject(MSGFESTIVALERROR);
                    }
                    return deferred.promise;
                },
                getChosenLineup:function(){
                    var deferred = $q.defer();

                    var choice = [];
                    var chosen = localStorageService.get('schedule');
                    if(chosen !== null){
                        _.each(chosen, function(id){
                            var schedule = _.find(chosen, function(lineup){
                                return lineup.id === id;
                            });
                            if(typeof schedule !== 'undefined')
                                choice.push(schedule);
                        });
                        deferred.resolve(chosen);
                    }else{
                        deferred.reject(MSGFESTIVALERROR);
                    }
                    return deferred.promise;
                },
                isFestivalAlreadyChosen:function(festivalId){
                    if(localStorageService.get('chosenfestival') === null)
                        return false;

                    var chosen = localStorageService.get('chosenfestival');

                    var festival = _.find(chosen, function(sId){
                        return sId === festivalId;
                    });

                    if(typeof festival === 'undefined')
                        return false;

                    return true;
                },
                isLineupAlreadyInSchedule:function(lineupId){
                    if(localStorageService.get('schedule') === null)
                        return false;

                    var chosen = localStorageService.get('schedule');

                    var lineup = _.find(chosen, function(sId){
                        return sId === lineupId;
                    });

                    if(typeof lineup === 'undefined')
                        return false;

                    return true;
                },
                isLineupNotInSchedule:function(lineupId){
                    if(localStorageService.get('schedule') === null)
                        return true;

                    var chosen = localStorageService.get('schedule');

                    var lineup = _.find(chosen, function(sId){
                        return sId === lineupId;
                    });

                    if(typeof lineup === 'undefined')
                        return true;

                    return false;
                },
                addFestivalToChoice:function(festivalId){
                    if(!this.isFestivalAlreadyChosen(festivalId)){
                        _chosenfestival = localStorageService.get('chosenfestival');

                        if(_chosenfestival === null){
                            _chosenfestival = [];
                        }

                        _chosenfestival = [festivalId];
                        localStorageService.set('chosenfestival', _chosenfestival);
                    }
                },
                removeFestivalFromChoice:function(festivalId){
                    if(this.isFestivalAlreadyChosen(festivalId)){
                        _chosenfestival = localStorageService.get('chosenfestival');

                        if(_chosenfestival !== null){
                            _chosenfestival = _.pull(_chosenfestival, festivalId);
                            localStorageService.set('chosenfestival', _chosenfestival);
                        }
                    }
                },
                addLineupToSchedule:function(lineupId){
                    if(!this.isLineupAlreadyInSchedule(lineupId)){
                        _schedule = localStorageService.get('schedule');

                        if(_schedule === null){
                            _schedule = [];
                        }

                        _schedule.push(lineupId);
                        localStorageService.set('schedule', _schedule);
                    }
                },
                removeLineupFromSchedule:function(lineupId){
                    console.log('klik');
                    if(this.isLineupAlreadyInSchedule(lineupId)){
                        _schedule = localStorageService.get('schedule');
                        console.log('klik');
                        if(_schedule !== null){
                            _schedule = _.pull(_schedule, lineupId);
                            localStorageService.set('schedule', _schedule);
                        }
                    }
                },
                getThemeColor:function(){
                    var deferred = $q.defer();

                    var choice = [];
                    var chosen = localStorageService.get('themecolor');
                    if(chosen !== null){
                        _.each(chosen, function(id){
                            var fest = _.find(chosen, function(color){
                                return color.id === id;
                            });
                            if(typeof fest !== 'undefined')
                                choice.push(fest);
                        });
                        deferred.resolve(chosen);
                    }else{
                        deferred.reject(MSGFESTIVALERROR);
                    }
                    return deferred.promise;
                },
                isThemeAlreadyChosen:function(colorId){
                    if(localStorageService.get('themecolor') === null)
                        return false;

                    var chosen = localStorageService.get('themecolor');

                    var color = _.find(chosen, function(sId){
                        return sId === colorId;
                    });

                    if(typeof color === 'undefined')
                        return false;

                    return true;
                },
                addThemeColor:function(colorId){
                    if(!this.isThemeAlreadyChosen(colorId)){
                        _themecolor = localStorageService.get('themecolor');

                        if(_themecolor === null){
                            _themecolor = [];
                        }

                        _themecolor = [colorId];
                        localStorageService.set('themecolor', _themecolor);
                    }
                },
                isColorInTheme:function(colorId){
                    if(localStorageService.get('themecolor') === null)
                        return false;

                    var chosen = localStorageService.get('themecolor');

                    var color = _.find(chosen, function(sId){
                        return sId === colorId;
                    });

                    if(typeof color === 'undefined')
                        return false;

                    return true;
                },
                isColorNotTheme:function(colorId){
                    if(localStorageService.get('themecolor') === null)
                        return true;

                    var chosen = localStorageService.get('themecolor');

                    var color = _.find(chosen, function(sId){
                        return sId === colorId;
                    });

                    if(typeof color === 'undefined')
                        return true;

                    return false;
                },
                removeThemeColor:function(colorId){
                    console.log('klik');
                    if(this.isLineupAlreadyInSchedule(colorId)){
                        _themecolor = localStorageService.get('schedule');
                        console.log('klik');
                        if(_themecolor !== null){
                            _themecolor = _.pull(_themecolor, colorId);
                            localStorageService.set('schedule', _themecolor);
                        }
                    }
                },
                getGEOLocation : function(){
                    var deferred = $q.defer();

                    $rootScope.$on('GEOSUCCESS', function(ev){
                        deferred.resolve(_geoPosition);
                    });

                    $rootScope.$on('GEOERROR', function(ev){
                        deferred.reject(MSGGEOLOCATIONNOTSUPPORTED);
                    });

                    that.geoLocation();

                    return deferred.promise;//Always return a promise
                }
            };
        }]);
})();